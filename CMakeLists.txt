cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME AUKF_TRA_PRE)
project(${PROJECT_NAME})

set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_CXX_FLAGS "-std=c++11")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -Wall -g")
set(AUKF_TRA_PRE_SOURCE_DIR
    src/sources)
set(AUKF_TRA_PRE_INCLUDE_DIR
    src/include)
set(AUKF_TRA_PRE_SOURCE_FILES
    ${AUKF_TRA_PRE_SOURCE_DIR}/aukf_tra_pre.cpp
    ${AUKF_TRA_PRE_SOURCE_DIR}/aukf.cpp
    ${AUKF_TRA_PRE_SOURCE_DIR}/predict.cpp)
 set(AUKF_TRA_PRE_HEADER_FILES
    ${AUKF_TRA_PRE_INCLUDE_DIR}/aukf_tra_pre.h
    ${AUKF_TRA_PRE_INCLUDE_DIR}/aukf.h
    ${AUKF_TRA_PRE_INCLUDE_DIR}/predict.h)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  rospy  
  robot_process
  droneMsgsROS
  opencv_apps
  geometry_msgs
  sensor_msgs
  nav_msgs
  visualization_msgs
  message_generation
  cv_bridge
  image_transport
)

#if OpenCV
find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)

generate_messages(
  DEPENDENCIES
  std_msgs
  geometry_msgs
)

###################################
## catkin specific configuration ##
catkin_package(
  CATKIN_DEPENDS roscpp std_msgs 
                 robot_process droneMsgsROS opencv_apps message_runtime
                 geometry_msgs nav_msgs 
                 sensor_msgs visualization_msgs cv_bridge image_transport
	         
  DEPENDS OpenCV Eigen
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
##include_directories(${EIGEN3_INCLUDE_DIR})
include_directories(
    ${AUKF_TRA_PRE_INCLUDE_DIR}
    ${catkin_INCLUDE_DIRS}
    ${EIGEN_INCLUDE_DIRS}
)

#include_directories(${OpenCV_INCLUDE_DIRS})
add_library(AUKF_TRA_PRE_lib ${AUKF_TRA_PRE_SOURCE_FILES} ${AUKF_TRA_PRE_HEADER_FILES})
add_dependencies(AUKF_TRA_PRE_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(AUKF_TRA_PRE_lib ${catkin_LIBRARIES} ${Eigen_LIBRARIES} ${OpenCV_LIBS})

add_executable(aukf_tra_pre_node src/sources/aukf_tra_pre_node.cpp)
add_dependencies(aukf_tra_pre_node ${catkin_EXPORTED_TARGETS})
target_link_libraries(aukf_tra_pre_node AUKF_TRA_PRE_lib ${OpenCV_LIBRARY})

add_executable(plot_res_node src/sources/plot_res_node.cpp)
add_dependencies(plot_res_node ${catkin_EXPORTED_TARGETS})
target_link_libraries(plot_res_node ${catkin_LIBRARIES} ${Eigen_LIBRARIES} ${OpenCV_LIBS} ${OpenCV_LIBRARY})
