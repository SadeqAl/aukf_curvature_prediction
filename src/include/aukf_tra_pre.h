#ifndef aukf_tra_pre_H
#define aukf_tra_pre_H

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include "cstdlib"

//egien
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/Core>

//opencv
#include <opencv/cv.h>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "opencv2/core/core.hpp"

// ROS
#include "ros/ros.h"

//Drone module
#include "robot_process.h"

//ROS message
#include "nav_msgs/Path.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PointStamped.h>

//DroneMsgsROS
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneSpeeds.h>
#include <droneMsgsROS/droneTrajectoryRefCommand.h>
#include <droneMsgsROS/droneRefCommand.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/robotPoseStampedVector.h>
#include <droneMsgsROS/robotPoseStamped.h>

//Gazebo messages
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/ModelState.h"
#include "gazebo_msgs/SetModelState.h"

#include "predict.h"

class aukf_tra_pre : public RobotProcess
{
    public:
    aukf_tra_pre();
    ~aukf_tra_pre();

    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr &msg);
    inline void publishPose(droneMsgsROS::robotPoseStampedVector &pose);
    inline void publishEst(droneMsgsROS::dronePose &pos);

   private:

    struct {
        ros::Time stamp_;
        double pose_x_, pose_y_, pose_z_;
    }object_state_;

    std::string stack_path_;
    std::string config_file_;
    std::string drone_id_;
    std::string odom_sim_pose_;
    std::string drone_predicted_pose_;
    std::string drone_updated_pose_;
    int limit_number;
    ros::NodeHandle n_;
    Eigen::MatrixXd X_;
    Eigen::VectorXd X_aukf_;
    Eigen::MatrixXd P_;
    Eigen::MatrixXd F_;
    Eigen::VectorXd X_Measure;

    bool received_odom_data_;
    bool new_updated_pose_;
    double timePrev_, timeNow_;
    double deltaT_;

    ros::Subscriber odom_pose_sim_sub_;
    ros::Publisher drone_tra_pre_pub_;
    ros::Publisher drone_tra_upd_pub_;
};

#endif
