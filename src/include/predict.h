#ifndef predict_H
#define predict_H

//egien
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/Core>

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <iostream>

//std
#include <string>
#include "cstdlib"

//opencv
#include <opencv/cv.h>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "opencv2/core/core.hpp"

#include "aukf.h"

class predict
{

   public:     
      predict();
      ~predict();
      void Traj_Prediction(Eigen::VectorXd &X_Pre, Eigen::MatrixXd &P_Pre, Eigen::MatrixXd &F_Pre, double deltaT,
                           std::vector<cv::Point3d> &State_Output_vector, double limit_number);
};

#endif

