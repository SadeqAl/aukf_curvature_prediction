#ifndef aukf_H

#define aukf_H


//egien
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/Core>

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include "cstdlib"

class aukf
{
   public:
   aukf();
   ~aukf();
   Eigen::MatrixXd Xsig_pred_;
   Eigen::MatrixXd Q_ = Eigen::MatrixXd(13,13);
   Eigen::MatrixXd R_ = Eigen::MatrixXd(3,3);
   int L_;
   int n_;
   int m_;
   double alpha_;
   double beta_;
   double kappa_;
   double lambda_;
   double c_;
   Eigen::VectorXd Wm_;
   Eigen::VectorXd Wc_;
   Eigen::MatrixXd GenerateSigmaPoints(Eigen::VectorXd X, Eigen::MatrixXd P);
   void Pre_SigmaPoints(Eigen::MatrixXd &Xsig_pre, Eigen::MatrixXd &F, Eigen::MatrixXd Xsig, double dt);
   void Adaptive_UKF(Eigen::MatrixXd Xsig_pre, Eigen::VectorXd &X, Eigen::MatrixXd &P);
   void AUKF_Prediction(Eigen::VectorXd &X, Eigen::MatrixXd &P, Eigen::MatrixXd &F, double dt);
   void Tra_Iteration(Eigen::VectorXd &X, double dt);
   void AUKF_Update(Eigen::VectorXd &X, Eigen::MatrixXd &P, Eigen::VectorXd &X_Measure);
};
#endif
