#include "predict.h"

predict::predict() {}
predict::~predict() {}

void predict::Traj_Prediction(Eigen::VectorXd &X_Pre, Eigen::MatrixXd &P_Pre, Eigen::MatrixXd &F_Pre, double deltaT,
                              std::vector<cv::Point3d> &predicted_vector, double limit_number)
{
    cv::Point3d predicted_point;
    aukf adaptive_ukf;
    Eigen::VectorXd X_local = X_Pre;

   for(int i = 0; i < limit_number; i++)
   {
     adaptive_ukf.AUKF_Prediction(X_local, P_Pre, F_Pre, deltaT);
     predicted_point.x = X_local(0);
     predicted_point.y = X_local(2);
     predicted_point.z = X_local(4);
     predicted_vector.push_back(predicted_point);
   }
}
