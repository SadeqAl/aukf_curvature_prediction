//I/O Stream
#include <iostream>
#include <string>
#include "ros/ros.h"

//std_messages
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Header.h>

//ros_messages
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/robotPoseStampedVector.h>
#include <droneMsgsROS/robotPoseStamped.h>

//Gazebo messages
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/ModelState.h"
#include "gazebo_msgs/SetModelState.h"

//opencv
#include <opencv/cv.h>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "opencv2/core/core.hpp"

ros::Publisher Lemniscate_tra_pub, predicted_tra_pub, updated_tra_pub;
visualization_msgs::Marker MAV_marker, pre_marker, upd_marker;

void display_mav_tra(cv::Point3d mav_pose)
{
    geometry_msgs::Point point;
    MAV_marker.header.stamp = ros::Time();
    MAV_marker.header.frame_id = "world";
    MAV_marker.ns = "MAV_TRA";
    MAV_marker.id = 200;
    MAV_marker.action = visualization_msgs::Marker::ADD;
    MAV_marker.type = visualization_msgs::Marker::LINE_STRIP;
    MAV_marker.pose.orientation.w = 1;
    MAV_marker.scale.x = 0.05;
    MAV_marker.color.a = 1.0;
    MAV_marker.color.r = 1;
    MAV_marker.color.g = 0;
    MAV_marker.color.b = 0;
    point.x = mav_pose.x;
    point.y = mav_pose.y;
    point.z = mav_pose.z;
    MAV_marker.points.push_back(point);
    Lemniscate_tra_pub.publish(MAV_marker);
}

void display_pre_tra(cv::Point3d mav_pose)
{
    geometry_msgs::Point point;
    pre_marker.header.stamp = ros::Time();
    pre_marker.header.frame_id = "world";
    pre_marker.ns = "PRE_TRA";
    pre_marker.id = 100;
    pre_marker.action = visualization_msgs::Marker::ADD;
    pre_marker.type = visualization_msgs::Marker::LINE_STRIP;
    pre_marker.pose.orientation.w = 1;
    pre_marker.scale.x = 0.05;
    pre_marker.color.a = 1.0;
    pre_marker.color.r = 1;
    pre_marker.color.g = 1;
    pre_marker.color.b = 0;
    point.x = mav_pose.x;
    point.y = mav_pose.y;
    point.z = mav_pose.z;
    pre_marker.points.push_back(point);
    predicted_tra_pub.publish(pre_marker);
}

void display_UPD_TRA(cv::Point3d mav_pose)
{
    geometry_msgs::Point point;
    upd_marker.header.stamp = ros::Time();
    upd_marker.header.frame_id = "world";
    upd_marker.ns = "UPD_TRA";
    upd_marker.id = 300;
    upd_marker.action = visualization_msgs::Marker::ADD;
    upd_marker.type = visualization_msgs::Marker::LINE_STRIP;
    upd_marker.pose.orientation.w = 1;
    upd_marker.scale.x = 0.05;
    upd_marker.color.a = 1.0;
    upd_marker.color.r = 0;
    upd_marker.color.g = 1;
    upd_marker.color.b = 0;
    point.x = mav_pose.x;
    point.y = mav_pose.y;
    point.z = mav_pose.z;
    upd_marker.points.push_back(point);
    updated_tra_pub.publish(upd_marker);
}

void drone_pose_callback(const gazebo_msgs::ModelStates::ConstPtr &msg)
{
    cv::Point3d mav_pose;
    for (int i = 0; i < msg->name.size(); i++)
    {
      if (msg->name[i].compare("moving_drone") == 0)
       {
          mav_pose.x = msg->pose[i].position.x;
          mav_pose.y = msg->pose[i].position.y;
          mav_pose.z = msg->pose[i].position.z;
          double tra_ = sqrt(std::pow(mav_pose.x,2) + std::pow(mav_pose.y,2));

          if(tra_ < 0.2)
          {
            MAV_marker.points.clear();
          }
       }
    }
    display_mav_tra(mav_pose);
}

void predicted_pose_callback(const droneMsgsROS::robotPoseStampedVector &msg)
{
  for(int i = 0; i < msg.robot_pose_vector.size(); i++)
  {
      cv::Point3d point_;
      point_.x = msg.robot_pose_vector[i].x;
      point_.y = msg.robot_pose_vector[i].y;
      point_.z = msg.robot_pose_vector[i].z;
      display_pre_tra(point_);
  }
  pre_marker.points.clear();

}

void updated_pose_callback(const droneMsgsROS::dronePose &msg)
{
    cv::Point3d mav_pose;
    mav_pose.x = msg.x;
    mav_pose.y = msg.y;
    mav_pose.z = msg.z;
    display_UPD_TRA(mav_pose);
    double tra_ = sqrt(std::pow(mav_pose.x,2) + std::pow(mav_pose.y,2));

    if(tra_ < 0.2)
    {
        upd_marker.points.clear();
    }
}

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "plot_res_node"); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n;
    ros::Subscriber position_from_gazebo = n.subscribe("/gazebo/model_states", 1, drone_pose_callback);
    ros::Subscriber uav_predicted_position = n.subscribe("/drone7/drone_predicted_pose", 1, predicted_pose_callback);
    ros::Subscriber uav_updated_position = n.subscribe("/drone7/drone_updated_pose", 1, updated_pose_callback);
    Lemniscate_tra_pub = n.advertise<visualization_msgs::Marker>("Lemniscate_shape_tra",1,true);
    predicted_tra_pub = n.advertise<visualization_msgs::Marker>("predicted_tra",1,true);
    updated_tra_pub = n.advertise<visualization_msgs::Marker>("updated_tra",1,true);
    ros::Rate r(30);
    //Loop -> Ashyncronous Module
    while(ros::ok())
    {
        ros::spin();
        r.sleep();
    }
    return 1;
}
