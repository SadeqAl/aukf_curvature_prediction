﻿#include "aukf_tra_pre.h"

aukf_tra_pre::aukf_tra_pre() {}
aukf_tra_pre::~aukf_tra_pre() {}

void aukf_tra_pre::ownSetUp()
{
    received_odom_data_ = false;
    //initializing the filter
    this->init();
}

void aukf_tra_pre::ownStop()
{
    received_odom_data_ = false;
}

void aukf_tra_pre::ownRun()
{
    std::vector<cv::Point3d> predicted_vector;
    droneMsgsROS::dronePose drone_pose;
    bool new_predicted_pose_ = false;

    timePrev_ = timeNow_;
    timeNow_ = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);
    deltaT_   = timeNow_ - timePrev_;
    double time_odom = (double) object_state_.stamp_.sec + ((double) object_state_.stamp_.nsec / (double) 1E9);
    double duration = timeNow_ - time_odom;
    aukf adaptive_ukf;
    predict aukf_prediction;
    adaptive_ukf.AUKF_Prediction(X_aukf_, P_, F_, deltaT_);

    if(received_odom_data_ && duration < 0.1)
    {
      Eigen::VectorXd X_Measure = Eigen::VectorXd(3);
      Eigen::VectorXd X_Pre;
      Eigen::MatrixXd P_Pre;
      Eigen::MatrixXd F_Pre;

      X_Measure(0) = object_state_.pose_x_;
      X_Measure(1) = object_state_.pose_y_;
      X_Measure(2) = object_state_.pose_z_;
      adaptive_ukf.AUKF_Update(X_aukf_,P_,X_Measure);
      drone_pose.x = X_aukf_(0,0);
      drone_pose.y = X_aukf_(2,0);
      drone_pose.z = X_aukf_(4,0);
      drone_tra_upd_pub_.publish(drone_pose);
      X_Pre = X_aukf_;
      P_Pre = P_;
      F_Pre = F_;
      aukf_prediction.Traj_Prediction(X_Pre, P_Pre, F_Pre, deltaT_, predicted_vector, limit_number);
      new_predicted_pose_  = true;
    }

    droneMsgsROS::robotPoseStampedVector predicted_pose_vector;
    droneMsgsROS::robotPoseStamped predicted_pose;

    if(new_predicted_pose_)
    {
        for(int num = 0; num < limit_number; num++)
        {
          predicted_pose.header.stamp = ros::Time::now() + ros::Duration((num)*deltaT_);
          predicted_pose.x = predicted_vector[num].x;
          predicted_pose.y = predicted_vector[num].y;
          predicted_pose.z = predicted_vector[num].z;
          predicted_pose.theta = 0;
          predicted_pose_vector.robot_pose_vector.push_back(predicted_pose);
        }
        publishPose(predicted_pose_vector);
    }
    predicted_pose_vector.robot_pose_vector.clear();
    predicted_vector.clear();
}

void aukf_tra_pre::init()
{
    ros::param::get("~stack_path", stack_path_);

    if(stack_path_.length() == 0)
        stack_path_ = "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack";

    ros::param::get("~drone_id", drone_id_);

    ros::param::get("~odom_sim_pose", odom_sim_pose_);

    if(odom_sim_pose_.length() == 0)
        odom_sim_pose_ = "/gazebo/model_states";

    ros::param::get("~drone_predicted_pose", drone_predicted_pose_);

    if(drone_predicted_pose_.length() == 0)
        drone_predicted_pose_ = "drone_predicted_pose";

    ros::param::get("~drone_updated_pose", drone_updated_pose_);

    if(drone_updated_pose_.length() == 0)
        drone_updated_pose_ = "drone_updated_pose_";

    ros::param::get("~limit_number", limit_number);

    if(limit_number == 0)
        limit_number = 300;

    X_aukf_ = Eigen::VectorXd(13);
    F_.setZero(13,13);
    X_.setZero(13,1);
    P_.setZero(13,13);
    X_aukf_.setZero(13);
    timeNow_ = 0;
}

void aukf_tra_pre::ownStart()
{
    odom_pose_sim_sub_ = n_.subscribe(odom_sim_pose_,1, &aukf_tra_pre::PoseVelocityCallback, this);
    drone_tra_pre_pub_ = n_.advertise<droneMsgsROS::robotPoseStampedVector>(drone_predicted_pose_, 1);
    drone_tra_upd_pub_ = n_.advertise<droneMsgsROS::dronePose>(drone_updated_pose_, 1);
}

void aukf_tra_pre::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr &msg)
{
    for (int i = 0; i < msg->name.size(); i++)
    {
        if (msg->name[i].compare("moving_drone") == 0)
        {
          object_state_.stamp_ = ros::Time::now();
          object_state_.pose_x_ = msg->pose[i].position.x;
          object_state_.pose_y_ = msg->pose[i].position.y;
          object_state_.pose_z_ = msg->pose[i].position.z;
          received_odom_data_ = true;
          break;
        }
    }
}

inline void aukf_tra_pre::publishPose(droneMsgsROS::robotPoseStampedVector &pose)
{
    drone_tra_pre_pub_.publish(pose);
}

inline void aukf_tra_pre::publishEst(droneMsgsROS::dronePose &pose)
{
    drone_tra_upd_pub_.publish(pose);
}
